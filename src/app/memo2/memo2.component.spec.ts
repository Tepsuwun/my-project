import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Memo2Component } from './memo2.component';

describe('Memo2Component', () => {
  let component: Memo2Component;
  let fixture: ComponentFixture<Memo2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Memo2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Memo2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
