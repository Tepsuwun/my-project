import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-memo2',
  templateUrl: './memo2.component.html',
  styleUrls: ['./memo2.component.css']
})
export class Memo2Component implements OnInit {
  memo : any = {};
  memos = [];
  isSubmitted : boolean = false;

  constructor() { }

  ngOnInit() {
    if(localStorage.getItem('memos')) {
      this.memos = JSON.parse(localStorage.getItem('memos'));
    }
  }

  onSubmit (memoForm: NgForm) {
    this.isSubmitted = true;
    if (memoForm.valid){
      this.memos.push({
        title : memoForm.controls.title.value,
        detail : memoForm.controls. detail.value
      });
      localStorage.setItem('memos', JSON.stringify(this.memos));
      this.isSubmitted = false;
      memoForm.resetForm();
    }
  }

  removeMemo (index) {
    this.memos.splice(index, 1);
    localStorage.setItem('memos', JSON.stringify(this.memos))
  }
}
