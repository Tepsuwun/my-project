import { Component, OnInit } from '@angular/core';

import { HttpService } from '../http.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  carousel_items = [
    {
      img: "assets/img/01.jpg",
      title: "รูปที่1",
      description: "assets/img/01.jpg"
    },
    {
      img: "assets/img/02.jpg",
      title: "รูปที่2",
      description: "assets/img/02.jpg"
    },
    {
      img: "assets/img/03.jpg",
      title: "รูปที่3",
      description: "assets/img/03.jpg"
    }

  ]

  de_cards = [
    {
      img: "assets/img/01.jpg",
      title: "รูปที่1",
      description: {
        short: "assets/img/01.jpg",
        full: "ที่อยู่โฟลเดอร์ assets/img/01.jpg",
      }
    },
    {
      img: "assets/img/02.jpg",
      title: "รูปที่2",
      description: {
        short: "assets/img/02.jpg",
        full: "ที่อยู่โฟลเดอร์ assets/img/02.jpg",
      }
    },
    {
      img: "assets/img/03.jpg",
      title: "รูปที่3",
      description: {
        short: "assets/img/03.jpg",
        full: "ที่อยู่โฟลเดอร์ assets/img/03.jpg",
      }
    },
    {
      img: "assets/img/04.jpg",
      title: "รูปที่4",
      description: {
        short: "assets/img/04.jpg",
        full: "ที่อยู่โฟลเดอร์ assets/img/04.jpg",
      }
    },
    {
      img: "assets/img/05.jpg",
      title: "รูปที่5",
      description: {
        short: "assets/img/05.jpg",
        full: "ที่อยู่โฟลเดอร์ assets/img/05.jpg",
      }
    },
    {
      img: "assets/img/06.jpg",
      title: "รูปที่6",
      description: {
        short: "assets/img/06.jpg",
        full: "ที่อยู่โฟลเดอร์ assets/img/06.jpg",
      }
    }
  ]

  cards : any;

  constructor(private http: HttpService) { }

  ngOnInit() {
    this.http.get('assets/card.json').subscribe((data) => {
      this.cards = data;
      localStorage.setItem('cards', JSON.stringify(data));
    });
  }

}
